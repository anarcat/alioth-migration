Alioth-GitLab migration scripts
===============================

This repository contains tools to facilitate the Alioth to GitLab
transition.

Assumed, prerequisted, is that you have shell access to Alioth.
(It is for disabling the Alioth repo by writting hooks files.)

More documentation about the migration in the [AliothMigratioh page in
the Debian wiki](https://wiki.debian.org/Salsa/AliothMigration).

disable-repository
------------------

Simple script written by Raphael Hertzog and heavily modified by
Antoine Beaupré to disable a repository on Alioth. It adds a hook that
denies pushes and a warning in the description.

migrate-repo
------------

Does the same, but attempts to import the repository into GitLab
first. Basically a rewrite of [Christopher Berg's shell
script](http://www.df7cb.de/blog/2017/Salsa_batch_import.html). It's faster because it works around that [pesky GitLab
bug](https://gitlab.com/gitlab-org/gitlab-ce/issues/42415) and it will wait until the repo is actually live before
disabling the repo on Alioth.

This should be self-documenting, but know that you will need to generate a
GitLab token and pass it through the `GITLAB_TOKEN` environment for the program
to work. See the `--help` argument for more information.

ls-not-migrated
---------------

Very dumb script to generate a series of shell commands that would call the
above migration script for all repos owned by the user.

It does not support anything outside of collab-maint: it hardcodes the "debian"
Salsa group on the other end and does not try to guess where else to send the
thing. This also means it doesn't support multi-level projects either.

You will likely run it like this:

    ~anarcat/bin/ls-not-migrated

Review the output, copy-paste a few entries (maybe adding --debug or --verbose
to see if things look right). Once you are satisfied this will work, just run
the whole remaining batch:

    ~anarcat/bin/ls-not-migrated | sh -x

This is dangerous. Do not do the above command without reviewing the output
without `sh -x` first.

This assumes the `GITLAB_TOKEN` environment is correctly set, see above.

missing bits
------------

 * modifying the `debian/control` file in relevant packages should
   still be done by hand, although it's technically possible to create
   a commit that would do that on alioth first

 * the [AliothRewriter](https://salsa.debian.org/salsa/AliothRewriter) step needs to be done by hand as well
